using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;

public class GhostRespawn : Agent
{
    [SerializeField] private Transform target;
    [SerializeField] private SpriteRenderer background;
    MovementController movementController;
    EnemyController enemyController;
    private GameObject[] ghostObjects;
    public GameObject Team;
    

    public override void OnEpisodeBegin()
    {
        movementController = GetComponent<MovementController>();
        enemyController = GetComponent<EnemyController>();      
    }

    public override void Heuristic(in ActionBuffers actionsOut)
    {
        ActionSegment<float> continuousActions = actionsOut.ContinuousActions;
        continuousActions[0] = Input.GetAxisRaw("Horizontal");
        continuousActions[1] = Input.GetAxisRaw("Vertical");
    }

   private void OnCollisionEnter2D(Collision2D collision) 
{
    GameObject[] ghostObjects = GameObject.FindGameObjectsWithTag("Ghost");

    foreach (GameObject ghostObject in ghostObjects)
    {
        EnemyController enemyController = ghostObject.GetComponent<EnemyController>();
        enemyController.ResetEnemy();
    }
}

    
}