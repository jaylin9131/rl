using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ButtonMouseCursor : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public Texture2D cursorTexture; // 选择样式的鼠标纹理
    public Vector2 hotSpot = Vector2.zero;

    private Button button;

    void Start()
    {
        button = GetComponent<Button>();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        UnityEngine.Cursor.SetCursor(cursorTexture, hotSpot, CursorMode.Auto);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        UnityEngine.Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
    }
    
    public void OnPointerExit(PointerEventData eventData)
    {
        UnityEngine.Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
    }
}
