using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    MovementController movementController;
    // Start is called before the first frame update
    public bool isAuto = false;
    float totalTime = 0f;
    int autoDirection = 0;
    void Awake()
    {
        movementController = GetComponent<MovementController>();
        
    }

    // Update is called once per frame
    void Update()
    {

        if(isAuto)
        {
            totalTime += Time.deltaTime;
            if(totalTime>=1)
            {
                autoDirection = Random.Range(0,4);
                totalTime = 0;
                if(autoDirection == 0) 
                {
                    movementController.setDirection("left");
                }
                else if(autoDirection == 1) 
                {
                    movementController.setDirection("right");
                }
                else if(autoDirection == 2)
                {
                    movementController.setDirection("up");
                }
                else if(autoDirection == 3)
                {
                    movementController.setDirection("down");
                }
                totalTime = 0f;
            }
            
        }
        else
        {
            if(Input.GetKey(KeyCode.A)) 
            {
                 movementController.setDirection("left");
            }
            if(Input.GetKey(KeyCode.D)) 
            {
                movementController.setDirection("right");
            }
            if(Input.GetKey(KeyCode.W))
            {
                movementController.setDirection("up");
            }
            if(Input.GetKey(KeyCode.S))
            {
                movementController.setDirection("down");
            }
        }
    }
}
