using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class VideoController : MonoBehaviour
{
    public VideoPlayer videoPlayer;
    public VideoClip secondVideoClip;
    public Button Button1,Button2,Button3;

    void Start()
    {
        PlayFirstVideo();

        Button1.onClick.AddListener(SwitchVideo);
        Button2.onClick.AddListener(SwitchVideo);
        Button3.onClick.AddListener(SwitchVideo);
    }

    void PlayFirstVideo()
    {
        
        videoPlayer.clip = videoPlayer.clip;
        videoPlayer.Play();
    }

    void SwitchVideo()
    {
        videoPlayer.Stop();

       
        videoPlayer.clip = secondVideoClip;
        videoPlayer.Play();
    }
}