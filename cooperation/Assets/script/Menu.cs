using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Single1()
    {
        SceneManager.LoadSceneAsync(1);
    }

    public void Single2()
    {
        SceneManager.LoadSceneAsync(2);
    }

    public void Dual()
    {
        SceneManager.LoadSceneAsync(3);
    }

    public void Training1()
    {
        SceneManager.LoadSceneAsync(4);
    }

    public void Training2()
    {
        SceneManager.LoadSceneAsync(5);
    }

    public void menu()
    {
        SceneManager.LoadSceneAsync(0);
    }


}
