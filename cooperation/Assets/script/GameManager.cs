using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    // Start is called before the first frame update
    public AgentEscape Agent;
    public AgentEscape Agent2;
    public GameObject Red;
    public GameObject Blue;
    public GameObject pauseMenu;

    private bool isPaused = false;

    private void Start()
    {
        Agent.agentGotCaught +=AgentDefeated;
        Agent2.agentGotCaught +=Agent2Defeated;
        Agent.agentGotCaught += bothEndEpisode;
        Agent2.agentGotCaught += bothEndEpisode;
        
    }

     private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Resume();
        }
    }


    // Update is called once per frame
    private void bothEndEpisode()
    {
        Agent.ResetEverything();
        Agent2.ResetEverything();
    }
    
    private void AgentDefeated()
    {
        Agent2.AddReward(-2f);
    }

    private void Agent2Defeated()
    {
        Agent.AddReward(-2f);
    }

    public void Resume()
    {
        isPaused = !isPaused;
        pauseMenu.SetActive(isPaused);
        Time.timeScale = isPaused ? 0f : 1f;
    }

    public void Menu()
    {
        pauseMenu.SetActive(false);
        Time.timeScale = 1f;
        isPaused = false;
        SceneManager.LoadScene(0);
    }

}
