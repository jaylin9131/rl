using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;
using System;

//REWARD 以每五秒增加2為基準去設計(可拆解)目前為0.5秒增加0.2
public class AgentEscape : Agent
{
    [SerializeField] private Transform target;
    [SerializeField] private Transform target1;
    [SerializeField] private Transform anotherAgent;
    public GameManager gameManager;
    MovementController movementController;
    Skill skill;
    Clock clock;
    public GameObject restartNode;
    StatsRecorder statsRecorder;

    private float lasthasReward = 0f , lastDistanceReward = 0f , laststopPositionReward = 0f;
    private bool isRun = true;

    private Vector2 laststopPosition;

    public event Action agentGotCaught;

    public int agentID;

    public override void OnEpisodeBegin()
    {
        movementController = GetComponent<MovementController>();
        ResetParameter();
        statsRecorder = Academy.Instance.StatsRecorder;
        skill = GetComponent<Skill>();
        clock = GetComponent<Clock>();
        target.localPosition = new Vector2(5f,3f);
        target1.localPosition = new Vector2(2f,3f);
        movementController.setrestartNode(restartNode);

        if(agentID == 1)
        {
            transform.localPosition =new  Vector2(-6.57f,-12.45f);
        }
        else if(agentID == 2)
        {
            transform.localPosition =new  Vector2(18.37f,-12.37f);
        }
        
    }

    public override void CollectObservations(VectorSensor sensor)
    {

        //全域偵測
        sensor.AddObservation((Vector2)transform.localPosition);
        sensor.AddObservation((Vector2)anotherAgent.localPosition);
        sensor.AddObservation((Vector2)target.localPosition);
        sensor.AddObservation((Vector2)target1.localPosition);
    }

    public override void OnActionReceived(ActionBuffers actions)
    {
        if(agentID==1)
        {
            if(actions.DiscreteActions[0]== 1 ||Input.GetKey(KeyCode.A)) 
            {
                movementController.setDirection("left");
            }
            if(actions.DiscreteActions[0]== 2 ||Input.GetKey(KeyCode.D)) 
            {
                movementController.setDirection("right");
            }
            if(actions.DiscreteActions[0]== 3 ||Input.GetKey(KeyCode.W))
            {
                movementController.setDirection("up");
            }
            if(actions.DiscreteActions[0]== 4 ||Input.GetKey(KeyCode.S))
            {
                movementController.setDirection("down");
            }
            if(actions.DiscreteActions[0]== 5 ||Input.GetKey(KeyCode.Space))
            {
                skill.useSkill();
            }

            RewardChanged();
            
        }
        else if(agentID==2)
        {
            if(actions.DiscreteActions[0]== 1 ||Input.GetKey(KeyCode.LeftArrow)) 
            {
                movementController.setDirection("left");
            }
            if(actions.DiscreteActions[0]== 2 ||Input.GetKey(KeyCode.RightArrow)) 
            {
                movementController.setDirection("right");
            }
            if(actions.DiscreteActions[0]== 3 ||Input.GetKey(KeyCode.UpArrow))
            {
                movementController.setDirection("up");
            }
            if(actions.DiscreteActions[0]== 4 ||Input.GetKey(KeyCode.DownArrow))
            {
                movementController.setDirection("down");
            }
            if(actions.DiscreteActions[0]== 5 ||Input.GetKey(KeyCode.RightControl))
            {
                skill.useSkill();
            }

            RewardChanged();
        }

        if (statsRecorder != null) 
        {
            statsRecorder.Add("Time survived",clock.timeCount);
        }

    }

    public override void Heuristic(in ActionBuffers actionsOut)
    {
        ActionSegment<float> continuousActions = actionsOut.ContinuousActions;
        continuousActions[0] = Input.GetAxisRaw("Horizontal");
        continuousActions[1] = Input.GetAxisRaw("Vertical");
    }

   private void OnCollisionEnter2D(Collision2D collision) 
    {

        if(collision.gameObject.CompareTag("Ghost"))
        {
            agentGotCaught?.Invoke();
        }

    }

    public void ShowReward()
    {
        float Reward = GetCumulativeReward();
        Debug.Log(gameObject.name + ":  " + Reward);
    }

    public void ResetEverything()
    {
        skill.ResetTrap();
        clock.ResetTime();
        AddReward(-10f);
        ShowReward();
        EndEpisode();    
    }

    public void ResetParameter()
    {
        movementController.speed = 5;
        lasthasReward = 0f;
        lastDistanceReward = 0f;
        laststopPositionReward = 0f;
        laststopPosition = transform.position;
        isRun = true;
    }

    public void RewardChanged()
    {
        if(clock.timeCount - lasthasReward >= 0.5f)
        {
            lasthasReward = clock.timeCount;
            AddReward(0.2f);
        }

        if(gameObject == skill.Owner && skill.TrapWho != null)
        {
            if(skill.TrapWho.CompareTag("Ghost"))
            {
                AddReward(0.8f);
            }
            else if(skill.TrapWho.CompareTag("Player") && skill.TrapWho != skill.Owner)
            {
                AddReward(2.5f);
            }
            else if(skill.TrapWho.CompareTag("Player") && skill.TrapWho == skill.Owner)
            {
                AddReward(-1.5f);
            }

            skill.TrapWho = null;
        }


        if(clock.timeCount - lastDistanceReward >= 1.5f)
        {
            lastDistanceReward = clock.timeCount;
            gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
            Vector2 RedP = gameManager.Red.transform.position;
            Vector2 BlueP = gameManager.Blue.transform.position;
            Vector2 AgentP = transform.position;
            float distancePurple = Vector2.Distance(RedP, AgentP);
            float distanceYellow = Vector2.Distance(BlueP,AgentP);
            float Distance = Mathf.Round(distancePurple + distanceYellow);
            AddReward(0.001f * Distance);

            if(distancePurple <= 3f || distanceYellow <= 3f)
            {
                AddReward(-0.25f);
                isRun = false;
            }
            else if(distancePurple <= 5f || distanceYellow <= 5f)
            {
                AddReward(-0.1f);
                isRun = false;
            }

            if(!isRun && distancePurple >= 5f && distanceYellow >= 5f)
            {
                AddReward(0.18f);
                isRun = true;
            }
        }

        if(clock.timeCount - laststopPositionReward >= 1.5f)
        {
            laststopPositionReward = clock.timeCount;
            Vector2 nowstopPosition = transform.position;
            float Distance = Vector2.Distance(laststopPosition,nowstopPosition);
            
            laststopPosition = nowstopPosition;
                
            if(Distance <= 1f)
            {
               AddReward(-0.7f);
            }
        }

    }
}