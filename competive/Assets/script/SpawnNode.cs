using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnNode : MonoBehaviour
{
    // Start is called before the first frame update
    int numTospawn = 18 ;
    float currentSpawnOffset;
    float spawnOffset = 1f;

    void Start()
    {
        // gameObject.name = "Node";
        // return;
        if(gameObject.name == "Node")
        {
            currentSpawnOffset = spawnOffset;
            for(int i = 0 ; i < numTospawn ; i++)
            {
                GameObject clone = Instantiate(gameObject,new Vector3(transform.position.x,transform.position.y+currentSpawnOffset,0),Quaternion.identity);
                currentSpawnOffset += spawnOffset;  
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    /*
    if(gameObject.name == "Node")
        {
            currentSpawnOffset = spawnOffset;
            for(int i = 0 ; i < numTospawn ; i++)
            {
                GameObject clone = Instantiate(gameObject,new Vector3(transform.position.x,transform.position.y+currentSpawnOffset,0),Quaternion.identity);
                currentSpawnOffset += spawnOffset;  
            }
        }
    */
}
