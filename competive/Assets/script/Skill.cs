using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill : MonoBehaviour
{
    private MovementController movementController;
    public GameObject SkillObject;
    private float cooldownTime = 10.0f;
    public bool canSkill = true;
    private float lastSkillTime = 0;
    private float SpeedRecord = 0;
    private float AgentSpeed = 5f;
    public GameObject Trap , TrapWho , Owner;
    private bool Speedcontrol = false;

    void Start()
    {
        movementController = GetComponent<MovementController>();
    }

    void Update()
    {
        if (!canSkill)
        {
            if (Time.time - lastSkillTime >= cooldownTime)
            {
                Owner = null;
                TrapWho = null;
                canSkill = true;
            }
        }

        if(Speedcontrol)
        {
            movementController.setSpeed(AgentSpeed);
            if (Time.time - lastSkillTime >= 0.8)
            {
                Speedcontrol = false;
            }
        }

    }

    public void useSkill()
    {
        if (canSkill)
        {
            Vector2 playerPosition = transform.position;
            Vector2 playerDirection = transform.up;
            Trap = Instantiate(SkillObject, playerPosition, Quaternion.identity);
            canSkill = false;
            lastSkillTime = Time.time;
            Speedcontrol = true;
            StartCoroutine(DestroyObjectAfterDelay(Trap, 5.0f));
        }
    }


    public void SkillEffect()
    {
        float speed = movementController.getSpeed();
        SpeedRecord = speed;
        movementController.setSpeed(speed * 0.7f);
    }

    public void ResetSpeed()
    {
        movementController.setSpeed(SpeedRecord);
    }

    private IEnumerator DestroyObjectAfterDelay(GameObject obj, float delay)
    {
        yield return new WaitForSeconds(delay);

        if (obj != null)
        {
            Destroy(obj);
            obj = null;
        }
    }

    public void setTrapWho(GameObject owner , GameObject trapwho)
    {
        Owner = owner;
        TrapWho = trapwho;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Trap"))
        {
            SkillEffect();
            Invoke("ResetSpeed", 3f);
        }
    }   

    public void ResetTrap()
    {
        Owner = null;
        TrapWho = null;
        lastSkillTime = 0;
        canSkill = true;
        ResetSpeed();
        StartCoroutine(DestroyObjectAfterDelay(Trap, 0.0f));
    }


}
