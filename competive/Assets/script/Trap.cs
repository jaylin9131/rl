using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trap : MonoBehaviour
{
    public GameObject lastTrappedObject = null;
    public GameObject Owner = null;
    private bool isOwner = true;


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (Owner == null)
        {
            Owner = other.gameObject;
            Skill ownerSkill = Owner.GetComponent<Skill>();
            ownerSkill.setTrapWho(Owner , lastTrappedObject);
            isOwner = false;
            return;
        }

        if (!isOwner)
        {
            lastTrappedObject = other.gameObject;
            Skill ownerSkill = Owner.GetComponent<Skill>();

            if (ownerSkill != null)
            {
                ownerSkill.setTrapWho(Owner , lastTrappedObject);
            }
        }
    }
}
