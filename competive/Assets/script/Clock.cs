using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Clock : MonoBehaviour
{
    public Text timeText;
    float timepass = 0f;
    float Seconds = 1.0f;
    float lastSeconds = 0f;
    public int timeCount = 0;
    void Start() 
    {
       
       
    }
    // Update is called once per frame
    void Update()
    {
        timepass += Time.deltaTime;
        if (timepass - lastSeconds >= Seconds)
        {
            timeCount = (int)timepass;
            lastSeconds = (float)timeCount;
            timeText.text = "Current Time\n" + timeCount.ToString();
        }
    }

    public void ResetTime()
    {
        timeCount = 0;
        timeText.text = "Current Time\n" + "0";
        timepass = 0f;
        lastSeconds = 0f;
    }

}
