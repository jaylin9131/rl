using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour
{
    public GameObject currentNode;
    public float speed = 4f;

    public string direction = "";
    public string lastMovingDirection = "";
    public bool isGhost = false;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Pellet currentNodeController = currentNode.GetComponent<Pellet>();
        transform.position = Vector2.MoveTowards(transform.position , currentNode.transform.position,speed*Time.deltaTime);
        
        bool reverseDiretion = false;
        if(
            (direction == "left" && lastMovingDirection == "right")
            ||(direction == "right" && lastMovingDirection == "left")
            ||(direction == "up" && lastMovingDirection == "down")
            ||(direction == "down" && lastMovingDirection == "up"))
            reverseDiretion = true;
    
        if((transform.position.x == currentNode.transform.position.x && transform.position.y == currentNode.transform.position.y) || reverseDiretion)
        {
            if(isGhost)
            {
                GetComponent<EnemyController>().ReachedCenterOfNode(currentNodeController);
            }
            GameObject newNode = currentNodeController.GetNodeFromDirection(direction);
            if(newNode!=null)
            {
                currentNode = newNode;
                lastMovingDirection = direction;
            }
            else
            {
                direction = lastMovingDirection;
                newNode = currentNodeController.GetNodeFromDirection(direction);
                if(newNode != null)
                {
                    currentNode = newNode;
                }
            }
        }
    }

    public void setDirection(string newDirection)
    {
        direction = newDirection;
    }

    public void setrestartNode(GameObject restartNode)
    {
        currentNode = restartNode;
    }

    public void setSpeed(float set_speed)
    {
        speed = set_speed;
    }

    public float getSpeed()
    {
        return speed;
    }
}

