using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public enum GhostNodeStatesEnum
    {
        leftNode,
        rightNode,
        centerNode,
        startNode,
        movingInNodes
    }
    public GhostNodeStatesEnum ghostNodeState;

    public enum GhostType{
        red,blue
    }
    
    public GhostType ghostType;
    public GameObject ghostNodeLeft;
    public GameObject ghostNodeRight;
    public GameObject ghostNodeCenter;
    public GameObject ghostNodeStart;
    public GameObject restartNode;
    public Transform Ghost;
    private MovementController movementController;
    public GameObject startingNode;
    private bool readytoLeaveHome = true;
    public GameManager gameManager;
    private float totalTimeEnemy = 0f;
    private float lastSpeedtime = 0f;
    private int chaseToken = 0;
    private float ChaseTime = 10;
    private float lastChaseTime = 0;
    private bool ChaseStatus = false;



    void Awake() {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        movementController = GetComponent<MovementController>();
        if(ghostType == GhostType.red)
        {
            ghostNodeState = GhostNodeStatesEnum.rightNode;
            startingNode = ghostNodeRight;
        }
        else if(ghostType == GhostType.blue)
        {
            ghostNodeState = GhostNodeStatesEnum.leftNode;
            startingNode = ghostNodeLeft;

        }
        movementController.currentNode = startingNode;
    }

    void Start()
    {
        
    }

    void Update()
    {
        EnemyAction();
    }

    public void ReachedCenterOfNode(Pellet nodeController)
    {
        if(ghostNodeState == GhostNodeStatesEnum.movingInNodes)
        {
            if(ghostType == GhostType.red)
            {
                if(!ChaseStatus) TracePurpleAgent();
                else TraceYellowAgent();
            }
            if(ghostType == GhostType.blue)
            {
                if(!ChaseStatus) TraceYellowAgent();
                else TracePurpleAgent();
            }
        }
        
        else if(readytoLeaveHome)
        {
            if(ghostNodeState == GhostNodeStatesEnum.leftNode)
            {
                ghostNodeState = GhostNodeStatesEnum.centerNode;
                movementController.setDirection("right");
            }
            else if(ghostNodeState == GhostNodeStatesEnum.rightNode)
            {
                ghostNodeState = GhostNodeStatesEnum.centerNode;
                movementController.setDirection("left");
            }
            else if(ghostNodeState == GhostNodeStatesEnum.centerNode)
            {
                ghostNodeState = GhostNodeStatesEnum.startNode;
                movementController.setDirection("up");
            }
            else if(ghostNodeState == GhostNodeStatesEnum.startNode)
            {
                ghostNodeState = GhostNodeStatesEnum.movingInNodes;
                movementController.setDirection("left");
            }
        }
    }

    void TracePurpleAgent ()
    {
        string direction = GetClosesDirection(gameManager.Agent.transform.position);
        movementController.setDirection(direction);
    }

    void TraceYellowAgent ()
    {
        string direction2 = GetClosesDirection(gameManager.Agent2.transform.position);
        movementController.setDirection(direction2);
    }

    string GetClosesDirection ( Vector2 target)
    {
        float shortestDistance = 0f;
        string lastMovingDirection = movementController.lastMovingDirection;
        string newDirection = "";
        Pellet nodeController = movementController.currentNode.GetComponent<Pellet>();
        
        if(nodeController.canMoveUp && lastMovingDirection != "down")
        {
            GameObject nodeUp = nodeController.nodeUp;

            float Distance = Vector2.Distance(nodeUp.transform.position,target);
            if(Distance < shortestDistance || shortestDistance <= 0)
            {
                shortestDistance = Distance;
                newDirection = "up";
            }
        }

        if(nodeController.canMoveDown && lastMovingDirection != "up")
        {
            GameObject nodeDown = nodeController.nodeDown;

            float Distance = Vector2.Distance(nodeDown.transform.position,target);
            if(Distance < shortestDistance || shortestDistance <= 0)
            {
                shortestDistance = Distance;
                newDirection = "down";
            }
        }

        if(nodeController.canMoveLeft && lastMovingDirection != "right")
        {
            GameObject nodeLeft = nodeController.nodeLeft;

            float Distance = Vector2.Distance(nodeLeft.transform.position,target);
            if(Distance < shortestDistance || shortestDistance <= 0)
            {
                shortestDistance = Distance;
                newDirection = "left";
            }
        }

        if(nodeController.canMoveRight && lastMovingDirection != "left")
        {
            GameObject nodeRight = nodeController.nodeRight;

            float Distance = Vector2.Distance(nodeRight.transform.position,target);
            if(Distance < shortestDistance || shortestDistance <= 0)
            {
                shortestDistance = Distance;
                newDirection = "right";
            }
        }
        return newDirection;
    }

    void EnemyAction()
    {
        totalTimeEnemy += Time.deltaTime;

        if(totalTimeEnemy - lastSpeedtime > ChaseTime)
        {
            lastSpeedtime = totalTimeEnemy;
            float speed = movementController.getSpeed();
            movementController.setSpeed(speed + 0.2f);
        }

        if(totalTimeEnemy - lastChaseTime >= ChaseTime)
        {
            ChaseTime = 5;
            lastChaseTime = totalTimeEnemy;
            bool nowChaseStatus = ChaseStatus;
            Vector2 PurpleP = gameManager.Agent.transform.position;
            Vector2 YellowP = gameManager.Agent2.transform.position;
            Vector2 GhostP = Ghost.position;
            float distancePurple = Vector2.Distance(PurpleP, GhostP);
            float distanceYellow = Vector2.Distance(YellowP, GhostP);
            
            if (distancePurple < distanceYellow)
            {
                if(ghostType == GhostType.red) ChaseStatus = false;
                else ChaseStatus = true;
            }
            else 
            {
                if(ghostType == GhostType.red) ChaseStatus = true;
                else ChaseStatus = false;
            }

            if(nowChaseStatus == ChaseStatus && chaseToken != 3)
            {
                chaseToken += 1;
            }
            if(nowChaseStatus == ChaseStatus && chaseToken == 3)
            {
                chaseToken = 0;
                ChaseStatus = !ChaseStatus;
            }
            else if(nowChaseStatus != ChaseStatus) 
            {
                chaseToken = 0;
            }
        }
    }

    public void ResetEnemy()
    {
        totalTimeEnemy = 0;
        lastSpeedtime = 0;
        chaseToken = 0;
        lastChaseTime = 0;
        ChaseTime = 10;
        ChaseStatus = false;
        movementController.setrestartNode(restartNode);
        movementController.setSpeed(4.0f);
    }
}
